# syntax=docker/dockerfile:1

FROM golang:1.23-bullseye AS build-stage

RUN go install github.com/veraison/cocli@4eada92
RUN go install github.com/veraison/evcli/v2@1685bf5
RUN go install github.com/veraison/gen-corim@corim-06

FROM busybox:1.35.0-uclibc AS busybox
FROM gcr.io/distroless/base-debian11 AS build-release-stage

WORKDIR /app

COPY --from=busybox /bin/sh /bin/sh
COPY --from=busybox /bin/mkdir /bin/mkdir
COPY --from=busybox /bin/cat /bin/cat

COPY --from=build-stage /go/bin/* /bin

COPY . .

CMD ["/app/endorse.sh"]
