# PoC Endorser

This repository contains a proof-of-concept endorser that creates reference values and verification key for the emulated CCA platform.
The reference values are extracted from a "golden" token.
The verification key is taken verbatim from a file containing the public CPAK.
The reference values and verification key are serialised as a CoRIM (Concise Reference Integrity Manifest), and sent to Veraison using the provisioning API.

## Build

Build the containerised endorser:

```sh
make build
```

## Configure

**NOTE**: The following assumes the Veraison services are available on the loopback interface - this is the case if you are running a docker instance of the Veraison services.  Tweak the addresses as needed to fit the deployment you are trying to provision.

Append the following two entries to your `/etc/hosts` file:

```
127.0.0.1 keycloak-service
127.0.0.1 provisioning-service
```

## Create and Submit Endorsements

```sh
make endorse
```

This will:

1. Create a CoMID with reference values from a golden token
1. Create a CoMID with verification key from a golden token and CPAK
1. Assemble a CoRIM with the two CoMIDs above
1. Send the CoRIM to Veraison's provisioning API endpoint

```mermaid
sequenceDiagram
    participant endorser
    participant Veraison

    endorser-->endorser: create CoRIM from "golden"<br>token and public CPAK
    endorser->>Veraison: POST /submit [ CoRIM ]
    Veraison->>endorser: 200 OK
```
