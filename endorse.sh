#!/bin/sh

set -eux

GOLDEN=cca-token.cbor
CPAK=cpak-pub.json
CORIM=cca-endorsements.cbor

gen-corim cca \
	${GOLDEN} \
	${CPAK} \
	--template-dir corim-templates \
	--corim-file ${CORIM}

cat << EOF > cocli.yaml
api_server: https://provisioning-service:8888/endorsement-provisioning/v1/submit
auth: oauth2
username: veraison-provisioner
password: veraison
client_id: veraison-client
client_secret: YifmabB4cVSPPtFLAmHfq7wKaEHQn10Z
token_url: https://keycloak-service:11111/realms/veraison/protocol/openid-connect/token
EOF

cocli corim submit \
	--ca-cert=/app/rootCA.crt \
	--corim-file=${CORIM} \
	--media-type="application/corim-unsigned+cbor; profile=\"http://arm.com/cca/ssd/1\"" \
	--config=cocli.yaml

[ $? = 0 ] || echo "endorsement submission failed: $?"
