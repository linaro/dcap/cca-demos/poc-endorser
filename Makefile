SHELL = /bin/bash

.DEFAULT_GOAL := all

TAG ?= "cca-demo/endorser"
VERAISON ?= name.invalid

all: build endorse

build: ; docker build --tag $(TAG) .
.PHONY: build

endorse: ; docker run --network=host $(TAG)
.PHONY: endorse

# list image contents
lsfs:
	rnd=$${RANDOM} ; \
	docker create --name="tmp_$${rnd}" $(TAG) \
	&& docker export tmp_$${rnd} | tar t \
	&& docker rm tmp_$${rnd}
.PHONY: lsfs
